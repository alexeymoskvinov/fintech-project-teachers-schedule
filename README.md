# fintech-project-teachers-schedule

>Stack of technologies used in my application:

- PostgreSQL as a database


- Test Containers


- Jpa for interaction between app and a db


- Heroku  for hosting

## Tutorial

Teachers can use this app to:

0. ***[must]*** Authorization
1. Create lessons
2. Create courses
3. Keep track of their schedule


### Authorization

1. Visit /registration
2. Send a **POST** request with your *full name*, *age*, *username*, *password*. And after registration you will get the administrator role
3. You now have a username and password which you must include in header in **any** request

> Possible errors:
> - /registration throws 400 error?
> You entered an existing username or a bad password

#### Creating lesson

1. Visit /lessons
2. You should create lesson entity;
3. Then you can edit the information about the lesson, assign a teacher to the lesson or remove something from your schedule  

#### Creating course

1. Visit /course
2. You should create course entity;
3. Next, you can edit the course information, assign a head teacher to the course, or remove something from your schedule.
4. It is also possible to copy a ready-made course for your purposes.

#### Teacher account

1. Visit /teachers
2. You can edit your profile or find the lesson schedule


## FAQ

> *Can I use app without signing up?*

No, app is configured in a way that doesn't allow usage without authorization. You can sign up and then sign in using
your username and password returned to you.

However, you can visit swagger-ui with no authorization to get acquainted with methods and their usage.


> *Is my password safely saved?*

Yes, application saves your password with encryption so that nobody can use it. When we get your log in request, we encrypt your typed password and compare it to the one saved in database.

