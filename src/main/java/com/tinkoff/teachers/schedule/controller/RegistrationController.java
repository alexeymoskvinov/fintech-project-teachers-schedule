package com.tinkoff.teachers.schedule.controller;

import com.tinkoff.teachers.schedule.dto.RegisteredTeacherDto;
import com.tinkoff.teachers.schedule.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/registration")
public class RegistrationController {

    private final TeacherService teacherService;

    @PostMapping
    public RegisteredTeacherDto addUser(@RequestBody @Valid RegisteredTeacherDto registeredTeacherDto) {
        return teacherService.register(registeredTeacherDto);
    }

}
