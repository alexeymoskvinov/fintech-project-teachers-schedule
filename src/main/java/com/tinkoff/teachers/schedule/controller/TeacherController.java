package com.tinkoff.teachers.schedule.controller;

import com.tinkoff.teachers.schedule.dto.LessonDto;
import com.tinkoff.teachers.schedule.dto.TeacherDto;
import com.tinkoff.teachers.schedule.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/teachers")
@RequiredArgsConstructor
public class TeacherController {
    private final TeacherService teacherService;

    @GetMapping("/{id}")
    public TeacherDto getById(@PathVariable("id") Long id) {
        return teacherService.getById(id);
    }

    @GetMapping
    public List<TeacherDto> get() {
        return teacherService.getAll();
    }

    @PutMapping("/{id}")
    @PreAuthorize("isThisTeacher(#id)")
    public TeacherDto update(@PathVariable("id") Long id, @RequestBody @Valid TeacherDto teacherDto) {
        return teacherService.update(id, teacherDto);
    }

    @DeleteMapping
    @PreAuthorize("isThisTeacher(#id)")
    public void deleteById(@RequestBody Long id) {
        teacherService.deleteById(id);
    }

    @GetMapping("/{id}/lessons")
    public List<LessonDto> getLessons(@PathVariable("id") Long id) {
        return teacherService.getLessonsByTeacherId(id);
    }
}
