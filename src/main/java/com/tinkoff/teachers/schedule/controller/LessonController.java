package com.tinkoff.teachers.schedule.controller;

import com.tinkoff.teachers.schedule.dto.LessonDto;
import com.tinkoff.teachers.schedule.service.LessonService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/lessons")
@RequiredArgsConstructor
public class LessonController {

    private final LessonService lessonService;

    @PostMapping
    public LessonDto create(@RequestBody @Valid LessonDto lessonDto) {
        return lessonService.add(lessonDto);
    }

    @GetMapping("/{id}")
    public LessonDto getById(@PathVariable("id") Long id) {
        return lessonService.getById(id);
    }

    @GetMapping
    public List<LessonDto> get() {
        return lessonService.getAll();
    }

    @PreAuthorize("isTeacherOnThisLesson(#id)")
    @PutMapping("/{id}")
    public LessonDto update(@PathVariable("id") Long id, @RequestBody @Valid LessonDto lessonDto) {
        return lessonService.update(id, lessonDto);
    }

    @DeleteMapping
    @PreAuthorize("isTeacherOnThisLesson(#id)")
    public void deleteById(@RequestBody Long id) {
        lessonService.deleteById(id);
    }

    @PutMapping("/{id}/teacher")
    @PreAuthorize("isTeacherOnThisLesson(#id)")
    public void updateTeacher(@PathVariable("id") Long id, @RequestBody Long teacherId) {
        lessonService.updateTeacher(id, teacherId);
    }
}
