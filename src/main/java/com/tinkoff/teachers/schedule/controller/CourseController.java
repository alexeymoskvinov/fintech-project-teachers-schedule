package com.tinkoff.teachers.schedule.controller;

import com.tinkoff.teachers.schedule.dto.CourseDto;
import com.tinkoff.teachers.schedule.service.CourseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/courses")
@RequiredArgsConstructor
@Slf4j
public class CourseController {

    private final CourseService courseService;

    @PostMapping
    public CourseDto create(@RequestBody @Valid CourseDto courseDto) {
        return courseService.add(courseDto);
    }

    @GetMapping("/{id}")
    public CourseDto getById(@PathVariable("id") Long id) {
        return courseService.getById(id);
    }

    @GetMapping
    public List<CourseDto> get() {
        return courseService.getAll();
    }

    @PreAuthorize("isTeacherOnThisCourse(#id)")
    @PutMapping("/{id}")
    public CourseDto update(@PathVariable("id") Long id, @RequestBody @Valid CourseDto courseDto) {
        return courseService.update(id, courseDto);
    }

    @DeleteMapping
    @PreAuthorize("isTeacherOnThisCourse(#id)")
    public void deleteById(@RequestBody Long id) {
        courseService.deleteById(id);
    }

    @PutMapping("/{id}/supervisor")
    @PreAuthorize("isTeacherOnThisCourse(#id)")
    public void updateSupervisor(@PathVariable("id") Long id, @RequestBody Long teacherId) {
        courseService.updateSupervisor(id, teacherId);
    }

    @PostMapping("/{id}/lessons")
    @PreAuthorize("isTeacherOnThisCourse(#id)")
    public void addLesson(@PathVariable("id") Long id, @RequestBody Long lessonId) {
        courseService.addLesson(id, lessonId);
    }

    @PostMapping("/{id}/make-copy")
    public CourseDto makeCopyById(@PathVariable("id") Long id) {
        return courseService.makeCopyById(id);
    }
}
