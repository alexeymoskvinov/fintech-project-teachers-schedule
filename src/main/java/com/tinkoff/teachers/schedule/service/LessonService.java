package com.tinkoff.teachers.schedule.service;

import com.tinkoff.teachers.schedule.dto.LessonDto;
import com.tinkoff.teachers.schedule.entity.Lesson;
import com.tinkoff.teachers.schedule.entity.Teacher;
import com.tinkoff.teachers.schedule.mapper.LessonMapper;
import com.tinkoff.teachers.schedule.repository.LessonRepository;
import com.tinkoff.teachers.schedule.security.MyTeacherDetails;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class LessonService {

    private final LessonRepository lessonRepository;
    private final LessonMapper lessonMapper;
    private final TeacherService teacherService;

    public LessonDto add(LessonDto lessonDto) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Teacher teacher = ((MyTeacherDetails) principal).getTeacher();
        Lesson lesson = lessonMapper.fromDto(lessonDto);
        lesson.setTeacher(teacher);
        lesson = lessonRepository.save(lesson);
        log.info("A Lesson with id = {} saved ", lesson.getId());
        return lessonMapper.fromEntity(lesson);
    }

    public Lesson findById(Long id) {
        return lessonRepository
                .findById(id)
                .orElseThrow(() -> {
                    log.error("Lesson not found by id = {}", id);
                    throw new EntityNotFoundException("Lesson not found by id=" + id);
                });
    }

    public LessonDto getById(Long id) {
        return lessonMapper.fromEntity(findById(id));
    }

    public List<LessonDto> getAll() {
        return lessonRepository
                .findAll()
                .stream()
                .map(lessonMapper::fromEntity)
                .toList();
    }

    @Transactional
    public LessonDto update(Long id, LessonDto lessonDto) {
        Lesson lesson = findById(id);
        lesson.setName(lessonDto.getName());
        lesson.setDescription(lessonDto.getDescription());
        lesson.setDateTime(lessonDto.getDateTime());
        lesson = lessonRepository.save(lesson);
        log.info("A lesson with id = {} has been updated", id);
        return lessonMapper.fromEntity(lesson);
    }

    @Transactional
    public void deleteById(Long id) {
        lessonRepository.deleteById(id);
        log.info("A lesson with id = {} has been deleted", id);
    }

    public void updateTeacher(Long id, Long teacherId) {
        Teacher teacher = teacherService.findById(teacherId);
        Lesson lesson = findById(id);
        lesson.setTeacher(teacher);
        lessonRepository.save(lesson);
        log.info("A teacher with id = {} has been added to the lesson with id = {}", teacherId, id);
    }
}
