package com.tinkoff.teachers.schedule.service;

import com.tinkoff.teachers.schedule.entity.Role;
import com.tinkoff.teachers.schedule.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
@RequiredArgsConstructor
@Slf4j
public class RoleService {
    private final RoleRepository roleRepository;

    public Role getByName(String name) {
        return roleRepository.findByName(name)
                .orElseThrow(() -> {
                    log.error("Role not found by name = {}", name);
                    throw new EntityNotFoundException("Not found role: " + name);
                });
    }
}
