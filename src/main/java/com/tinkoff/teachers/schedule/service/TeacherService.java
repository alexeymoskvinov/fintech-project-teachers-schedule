package com.tinkoff.teachers.schedule.service;

import com.tinkoff.teachers.schedule.dto.LessonDto;
import com.tinkoff.teachers.schedule.dto.RegisteredTeacherDto;
import com.tinkoff.teachers.schedule.dto.TeacherDto;
import com.tinkoff.teachers.schedule.entity.Teacher;
import com.tinkoff.teachers.schedule.mapper.LessonMapper;
import com.tinkoff.teachers.schedule.mapper.RegisteredTeacherMapper;
import com.tinkoff.teachers.schedule.mapper.TeacherMapper;
import com.tinkoff.teachers.schedule.repository.TeacherRepository;
import com.tinkoff.teachers.schedule.security.MyTeacherDetails;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class TeacherService implements UserDetailsService {

    private final TeacherRepository teacherRepository;
    private final TeacherMapper teacherMapper;
    private final RegisteredTeacherMapper registeredTeacherMapper;
    private final LessonMapper lessonMapper;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public RegisteredTeacherDto register(RegisteredTeacherDto registeredTeacherDto) {
        Teacher teacher = registeredTeacherMapper.fromDto(registeredTeacherDto);
        teacher.setRole(roleService.getByName("TEACHER"));
        teacher.setPassword(passwordEncoder.encode(registeredTeacherDto.getPassword()));
        teacher = teacherRepository.save(teacher);
        log.info("A teacher with id = {} saved ", teacher.getId());
        return registeredTeacherMapper.fromEntity(teacher);

    }

    public Teacher findById(Long id) {
        return teacherRepository
                .findById(id)
                .orElseThrow(() -> {
                    log.error("Teacher not found by id = {}", id);
                    throw new EntityNotFoundException("Teacher not found by id=" + id);
                });
    }

    public TeacherDto getById(Long id) {
        return teacherMapper.fromEntity(findById(id));
    }

    @Transactional
    public List<TeacherDto> getAll() {
        return teacherRepository
                .findAll()
                .stream()
                .map(teacherMapper::fromEntity)
                .toList();
    }

    @Transactional
    public TeacherDto update(Long id, TeacherDto teacherDto) {
        Teacher teacher = findById(id);
        teacher.setFullName(teacherDto.getFullName());
        teacher.setAge(teacherDto.getAge());
        teacher = teacherRepository.save(teacher);
        log.info("A teacher with id = {} has been updated", id);
        return teacherMapper.fromEntity(teacher);
    }

    @Transactional
    public void deleteById(Long id) {
        teacherRepository.deleteById(id);
        log.info("A teacher with id = {} has been deleted", id);
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Teacher teacher = teacherRepository
                .findByUsername(username)
                .orElseThrow(() -> {
                    log.error("Teacher not found by username = {}", username);
                    throw new UsernameNotFoundException("Not found Teacher with username: " + username);
                });
        return new MyTeacherDetails(teacher);
    }

    public boolean existsTeacherByUsername(String username) {
        return teacherRepository.existsTeacherByUsername(username);
    }

    @Transactional
    public List<LessonDto> getLessonsByTeacherId(Long id) {
        return findById(id)
                .getLessons()
                .stream()
                .map(lessonMapper::fromEntity)
                .sorted(Comparator.comparing(LessonDto::getDateTime, Comparator.nullsLast(Comparator.naturalOrder())))
                .toList();
    }
}
