package com.tinkoff.teachers.schedule.service;

import com.tinkoff.teachers.schedule.dto.CourseDto;
import com.tinkoff.teachers.schedule.dto.LessonDto;
import com.tinkoff.teachers.schedule.entity.Course;
import com.tinkoff.teachers.schedule.entity.Lesson;
import com.tinkoff.teachers.schedule.entity.Teacher;
import com.tinkoff.teachers.schedule.mapper.CourseMapper;
import com.tinkoff.teachers.schedule.mapper.LessonMapper;
import com.tinkoff.teachers.schedule.repository.CourseRepository;
import com.tinkoff.teachers.schedule.security.MyTeacherDetails;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class CourseService {

    private final CourseRepository courseRepository;
    private final CourseMapper courseMapper;
    private final TeacherService teacherService;
    private final LessonService lessonService;
    private final LessonMapper lessonMapper;

    @Transactional
    public CourseDto add(CourseDto courseDto) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Teacher supervisor = ((MyTeacherDetails) principal).getTeacher();
        Course course = courseMapper.fromDto(courseDto);
        course.setSupervisor(supervisor);
        course = courseRepository.save(course);
        log.info("A Course with id = {} saved ", course.getId());
        return courseMapper.fromEntity(course);

    }

    private Course findById(Long id) {
        return courseRepository
                .findById(id)
                .orElseThrow(() -> {
                    log.error("Course not found by id = {}", id);
                    throw new EntityNotFoundException("Course not found by id = " + id);
                });
    }

    public CourseDto getById(Long id) {
        return courseMapper.fromEntity(findById(id));
    }

    public List<CourseDto> getAll() {
        return courseRepository
                .findAll()
                .stream()
                .map(courseMapper::fromEntity)
                .toList();
    }


    @Transactional
    public CourseDto update(Long id, CourseDto courseDto) {
        Course course = findById(id);
        course.setName(courseDto.getName());
        course.setDescription(courseDto.getDescription());
        course.setCategory(courseDto.getCategory());
        course.setType(courseDto.getType());
        course.setNumberOfStudents(courseDto.getNumberOfStudents());
        course.setAdditionalInformation(courseDto.getAdditionalInformation());
        courseRepository.save(course);
        log.info("A Course with id = {} has been updated", id);
        return courseMapper.fromEntity(course);
    }

    @Transactional
    public void deleteById(Long id) {
        courseRepository.deleteById(id);
        log.info("A Course with id = {} has been deleted", id);
    }

    @Transactional
    public void updateSupervisor(Long id, Long supervisorId) {
        Teacher supervisor = teacherService.findById(supervisorId);
        Course course = findById(id);
        course.setSupervisor(supervisor);
        courseRepository.save(course);
        log.info("A supervisor with id = {} has been added to the course with id = {}", supervisorId, id);
    }

    @Transactional
    public void addLesson(Long id, Long lessonId) {
        Lesson lesson = lessonService.findById(lessonId);
        Course course = findById(id);
        course.getLessons().add(lesson);
        lesson.setCourse(course);
        lessonService.update(lessonId, lessonMapper.fromEntity(lesson));
        courseRepository.save(course);
        log.info("A lesson with id = {} has been added to the course with id = {}", lessonId, id);
    }

    private List<LessonDto> getLessons(Long id) {
        return findById(id)
                .getLessons()
                .stream()
                .map(lessonMapper::fromEntity)
                .toList();
    }

    @Transactional
    public CourseDto makeCopyById(Long id) {
        CourseDto courseDto = getById(id);
        courseDto.setId(null);
        courseDto = add(courseDto);
        Course course = courseMapper.fromDto(courseDto);
        List<LessonDto> lessons = getLessons(id);
        for (LessonDto lessonDto : lessons) {
            lessonDto.setId(null);
            lessonDto.setDateTime(null);
            lessonDto.setCourseDto(courseDto);
            course.getLessons().add(lessonMapper.fromDto(lessonService.add(lessonDto)));
        }
        course = courseRepository.save(course);
        log.info("A course with id = {} has been created by copy of course with id = {}", course.getId(), id);
        return courseMapper.fromEntity(course);
    }
}
