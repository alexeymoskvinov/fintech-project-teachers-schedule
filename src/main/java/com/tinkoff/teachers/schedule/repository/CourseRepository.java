package com.tinkoff.teachers.schedule.repository;

import com.tinkoff.teachers.schedule.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
}
