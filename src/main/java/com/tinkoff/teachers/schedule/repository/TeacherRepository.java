package com.tinkoff.teachers.schedule.repository;

import com.tinkoff.teachers.schedule.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Long> {
    Optional<Teacher> findByUsername(String username);

    boolean existsTeacherByUsername(String username);
}
