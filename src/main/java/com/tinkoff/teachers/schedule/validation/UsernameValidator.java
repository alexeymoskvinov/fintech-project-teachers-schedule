package com.tinkoff.teachers.schedule.validation;

import com.tinkoff.teachers.schedule.service.TeacherService;
import lombok.RequiredArgsConstructor;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@RequiredArgsConstructor
public class UsernameValidator implements ConstraintValidator<UsernameConstraint, String> {

    private final TeacherService teacherService;

    @Override
    public boolean isValid(String username, ConstraintValidatorContext constraintValidatorContext) {
        return !teacherService.existsTeacherByUsername(username);
    }
}
