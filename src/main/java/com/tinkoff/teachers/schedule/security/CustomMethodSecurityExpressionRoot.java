package com.tinkoff.teachers.schedule.security;

import com.tinkoff.teachers.schedule.dto.CourseDto;
import com.tinkoff.teachers.schedule.dto.LessonDto;
import com.tinkoff.teachers.schedule.entity.Teacher;
import com.tinkoff.teachers.schedule.service.CourseService;
import com.tinkoff.teachers.schedule.service.LessonService;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;

public class CustomMethodSecurityExpressionRoot
        extends SecurityExpressionRoot implements MethodSecurityExpressionOperations {

    private final LessonService lessonService;
    private final CourseService courseService;

    public CustomMethodSecurityExpressionRoot(Authentication authentication, LessonService lessonService, CourseService courseService) {
        super(authentication);
        this.lessonService = lessonService;
        this.courseService = courseService;
    }

    private boolean isAdmin(Teacher teacher) {
        return teacher.getRole().getName().equals("ADMIN");
    }

    public boolean isTeacherOnThisLesson(Long id) {
        Teacher teacher = ((MyTeacherDetails) this.getPrincipal()).getTeacher();
        LessonDto lessonDto = lessonService.getById(id);
        return isAdmin(teacher) ||
                lessonDto.getTeacherDto() == null ||
                lessonDto.getTeacherDto().getId().equals(teacher.getId());
    }

    public boolean isTeacherOnThisCourse(Long id) {
        Teacher teacher = ((MyTeacherDetails) this.getPrincipal()).getTeacher();
        CourseDto courseDto = courseService.getById(id);
        return isAdmin(teacher) ||
                courseDto.getSupervisor() == null ||
                courseDto.getSupervisor().getId().equals(teacher.getId());
    }

    public boolean isThisTeacher(Long id) {
        Teacher teacher = ((MyTeacherDetails) this.getPrincipal()).getTeacher();
        return (teacher.getRole().getName().equals("ADMIN") ||
                teacher.getId().equals(id));
    }

    @Override
    public void setFilterObject(Object filterObject) {
    }

    @Override
    public Object getFilterObject() {
        return null;
    }

    @Override
    public void setReturnObject(Object returnObject) {

    }

    @Override
    public Object getReturnObject() {
        return null;
    }

    @Override
    public Object getThis() {
        return null;
    }
}
