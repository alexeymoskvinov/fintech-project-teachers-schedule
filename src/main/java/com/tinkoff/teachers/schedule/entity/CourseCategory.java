package com.tinkoff.teachers.schedule.entity;

public enum CourseCategory {
    CATEGORY(null),
    TECHNICAL(CATEGORY),
    MATH(TECHNICAL),
    INFORMATICS(TECHNICAL),
    PHYSIC(TECHNICAL),
    HUMANITARIAN(CATEGORY),
    FOREIGN_LANGUAGE(HUMANITARIAN),
    NATURAL(CATEGORY),
    BIOLOGY(NATURAL),
    CHEMISTRY(NATURAL);

    private CourseCategory parent = null;

    CourseCategory(CourseCategory courseCategory) {
        this.parent = courseCategory;
    }

    public boolean is(CourseCategory other) {
        if (other == null) {
            return false;
        }

        for (CourseCategory t = this; t != null; t = t.parent) {
            if (other == t) {
                return true;
            }
        }
        return false;
    }

}
