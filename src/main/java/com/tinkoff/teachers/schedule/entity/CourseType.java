package com.tinkoff.teachers.schedule.entity;

public enum CourseType {
    ONLINE,
    OFFLINE
}
