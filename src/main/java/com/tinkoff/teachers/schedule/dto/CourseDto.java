package com.tinkoff.teachers.schedule.dto;

import com.tinkoff.teachers.schedule.entity.CourseCategory;
import com.tinkoff.teachers.schedule.entity.CourseType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@Data
@NoArgsConstructor
public class CourseDto {

    private Long id;

    @Size(min = 3, max = 200, message = "Name must be between 3 and 200 characters")
    @NotNull(message = "Name cannot be null")
    private String name;

    @Size(min = 10, max = 200, message = "Description Me must be between 10 and 200 characters")
    private String description;

    private CourseCategory category;

    @Nullable
    private TeacherDto supervisor;

    @Min(value = 0, message = "Number of students should not be less than 0")
    @Max(value = 150, message = "Number of students should not be greater than 150")
    private int numberOfStudents;

    private CourseType type;

    @Size(min = 10, max = 200, message = "Additional information Me must be between 10 and 200 characters")
    private String additionalInformation;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CourseDto courseDto = (CourseDto) o;
        return Objects.equals(id, courseDto.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
