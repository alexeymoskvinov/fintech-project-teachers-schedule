package com.tinkoff.teachers.schedule.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Objects;

@Data
@NoArgsConstructor
public class LessonDto {

    private Long id;

    @Size(min = 3, max = 200, message = "Name must be between 3 and 200 characters")
    @NotNull(message = "Name cannot be null")
    private String name;

    @Size(min = 10, max = 200, message = "Description Me must be between 10 and 200 characters")
    private String description;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dateTime;

    @Nullable
    private TeacherDto teacherDto;

    @JsonIgnore
    @Nullable
    private CourseDto courseDto;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LessonDto lessonDto = (LessonDto) o;
        return Objects.equals(id, lessonDto.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
