package com.tinkoff.teachers.schedule.dto;

import com.tinkoff.teachers.schedule.validation.PasswordConstraint;
import com.tinkoff.teachers.schedule.validation.UsernameConstraint;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class RegisteredTeacherDto {
    private Long id;

    @Size(min = 10, max = 200, message = "Full name must be between 10 and 200 characters")
    @NotNull(message = "Name cannot be null")
    private String fullName;

    @Min(value = 18, message = "Age should not be less than 18")
    @Max(value = 150, message = "Age should not be greater than 150")
    private int age;

    @NotNull(message = "username cannot be null")
    @UsernameConstraint
    private String username;

    @NotNull(message = "password cannot be null")
    @PasswordConstraint
    private String password;

    @Nullable
    private RoleDto roleDto;
}
