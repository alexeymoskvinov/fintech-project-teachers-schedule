package com.tinkoff.teachers.schedule.mapper;

import com.tinkoff.teachers.schedule.dto.LessonDto;
import com.tinkoff.teachers.schedule.entity.Lesson;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {CourseMapper.class, TeacherMapper.class})
public interface LessonMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "dateTime", source = "dateTime")
    @Mapping(target = "courseDto", source = "course")
    @Mapping(target = "teacherDto", source = "teacher")
    LessonDto fromEntity(Lesson lesson);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "dateTime", source = "dateTime")
    @Mapping(target = "course", source = "courseDto")
    @Mapping(target = "teacher", source = "teacherDto")
    Lesson fromDto(LessonDto lessonDto);
}
