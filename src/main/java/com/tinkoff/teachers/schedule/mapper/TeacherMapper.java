package com.tinkoff.teachers.schedule.mapper;

import com.tinkoff.teachers.schedule.dto.TeacherDto;
import com.tinkoff.teachers.schedule.entity.Teacher;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface TeacherMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "fullName", source = "fullName")
    @Mapping(target = "age", source = "age")
    TeacherDto fromEntity(Teacher teacher);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "fullName", source = "fullName")
    @Mapping(target = "age", source = "age")
    Teacher fromDto(TeacherDto teacherDto);
}
