package com.tinkoff.teachers.schedule.mapper;

import com.tinkoff.teachers.schedule.dto.CourseDto;
import com.tinkoff.teachers.schedule.entity.Course;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {TeacherMapper.class})
public interface CourseMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "category", source = "category")
    @Mapping(target = "numberOfStudents", source = "numberOfStudents")
    @Mapping(target = "type", source = "type")
    @Mapping(target = "supervisor", source = "supervisor")
    @Mapping(target = "additionalInformation", source = "additionalInformation")
    CourseDto fromEntity(Course course);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "category", source = "category")
    @Mapping(target = "numberOfStudents", source = "numberOfStudents")
    @Mapping(target = "type", source = "type")
    @Mapping(target = "additionalInformation", source = "additionalInformation")
    Course fromDto(CourseDto courseDto);
}
