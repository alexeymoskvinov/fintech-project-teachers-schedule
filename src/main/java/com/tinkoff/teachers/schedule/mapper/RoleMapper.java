package com.tinkoff.teachers.schedule.mapper;

import com.tinkoff.teachers.schedule.dto.RoleDto;
import com.tinkoff.teachers.schedule.entity.Role;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface RoleMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    Role fromDto(RoleDto roleDto);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    RoleDto fromEntity(Role role);
}
