package com.tinkoff.teachers.schedule.mapper;

import com.tinkoff.teachers.schedule.dto.RegisteredTeacherDto;
import com.tinkoff.teachers.schedule.entity.Teacher;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RoleMapper.class})
public interface RegisteredTeacherMapper {
    @Mapping(target = "id", source = "id")
    @Mapping(target = "fullName", source = "fullName")
    @Mapping(target = "age", source = "age")
    @Mapping(target = "roleDto", source = "role")
    @Mapping(target = "username", source = "username")
    @Mapping(target = "password", source = "password")
    RegisteredTeacherDto fromEntity(Teacher teacher);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "fullName", source = "fullName")
    @Mapping(target = "age", source = "age")
    @Mapping(target = "role", source = "roleDto")
    @Mapping(target = "username", source = "username")
    @Mapping(target = "password", source = "password")
    Teacher fromDto(RegisteredTeacherDto registeredTeacherDto);
}
