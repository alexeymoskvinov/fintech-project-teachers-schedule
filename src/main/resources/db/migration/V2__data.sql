insert into schedule_schema.role (id, name)
values (1, 'ADMIN');


insert into schedule_schema.role (id, name)
values (2, 'TEACHER');


insert into schedule_schema.teacher (full_name, age, username, password, role_id)
values ('admin', 0, 'admin', '$2a$12$0t9gr1NRKOEwkbx6UZhUf.4Gcj7AEW0aQHIF4mg39qe0jpD1JQ5C6', 1);
;;