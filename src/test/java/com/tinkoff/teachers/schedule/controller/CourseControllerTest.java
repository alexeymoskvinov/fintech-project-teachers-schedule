package com.tinkoff.teachers.schedule.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tinkoff.teachers.schedule.ApplicationIntegrationTest;
import com.tinkoff.teachers.schedule.dto.CourseDto;
import com.tinkoff.teachers.schedule.entity.Course;
import com.tinkoff.teachers.schedule.entity.Lesson;
import com.tinkoff.teachers.schedule.entity.Teacher;
import com.tinkoff.teachers.schedule.mapper.CourseMapper;
import com.tinkoff.teachers.schedule.repository.CourseRepository;
import com.tinkoff.teachers.schedule.repository.LessonRepository;
import com.tinkoff.teachers.schedule.repository.TeacherRepository;
import com.yannbriancon.interceptor.HibernateQueryCountInterceptor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CourseControllerTest extends ApplicationIntegrationTest {

    private MockMvc mvc;

    @Autowired
    private HibernateQueryCountInterceptor hibernateQueryCountInterceptor;

    @Autowired
    WebApplicationContext webApplicationContext;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private CourseMapper courseMapper;

    @Autowired
    private LessonRepository lessonRepository;

    private Course course;

    @Autowired
    private TeacherRepository teacherRepository;

    private List<Long> teacherIdsToRemove;

    @BeforeEach
    void setUp() {
        teacherIdsToRemove = new ArrayList<>();
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        course = new Course();
        course.setName("course controller test course");
        course = courseRepository.save(course);
        hibernateQueryCountInterceptor.removeCounter();
    }

    @AfterEach
    void tearDown() {
        lessonRepository.deleteAll();
        courseRepository.deleteAll();
        teacherRepository.deleteAllById(teacherIdsToRemove);
    }

    @Test
    @WithUserDetails("admin")
    void addCourseSuccessful() throws Exception {
        hibernateQueryCountInterceptor.startCounter();
        CourseDto courseDto = new CourseDto();
        courseDto.setName("add course test");
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        String uri = "/courses";
        String inputJson = mapToJson(courseDto);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();

        courseDto = mapFromJson(content, CourseDto.class);
        assertEquals(1, hibernateQueryCountInterceptor.getQueryCount());
        assertTrue(courseRepository.existsById(courseDto.getId()));
    }

    @Test
    void getCourseByIdSuccessful() throws Exception {
        String uri = "/courses/%s";
        hibernateQueryCountInterceptor.startCounter();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(String.format(uri, course.getId()))
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();

        assertEquals(1, hibernateQueryCountInterceptor.getQueryCount());
        assertEquals(courseMapper.fromEntity(course), mapFromJson(content, CourseDto.class));
    }

    @Test
    void getAllCoursesSuccessful() throws Exception {
        String uri = "/courses";
        hibernateQueryCountInterceptor.startCounter();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();

        assertEquals(1, hibernateQueryCountInterceptor.getQueryCount());
        assertEquals(mapToJson(List.of(courseMapper.fromEntity(course))), content);
    }

    @Test
    @WithUserDetails("admin")
    void updateCourseByIdAndDtoSuccessful() throws Exception {
        course.setName("New course name");
        String uri = "/courses/%s";
        hibernateQueryCountInterceptor.startCounter();
        String inputJson = mapToJson(courseMapper.fromEntity(course));
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(String.format(uri, course.getId()), inputJson)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();

        assertEquals(2, hibernateQueryCountInterceptor.getQueryCount());
        assertEquals("New course name", mapFromJson(content, CourseDto.class).getName());
    }

    @Test
    @WithUserDetails("admin")
    void deleteCourseByIdSuccessful() throws Exception {
        String uri = "/courses";
        String inputJson = mapToJson(course.getId());
        hibernateQueryCountInterceptor.startCounter();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(3, hibernateQueryCountInterceptor.getQueryCount());
        assertEquals(200, status);
        assertEquals(0, courseRepository.findAll().size());
    }

    @Test
    @WithUserDetails("admin")
    void updateSupervisorOnCourseByIdAndTeacherIdSuccessful() throws Exception {
        Teacher teacher1 = new Teacher();
        teacher1.setFullName("testing teacher");
        teacher1.setAge(20);
        teacher1 = teacherRepository.save(teacher1);

        String uri = "/courses/%s/supervisor";
        String inputJson = mapToJson(teacher1.getId());
        hibernateQueryCountInterceptor.startCounter();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(String.format(uri, course.getId()))
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(3, hibernateQueryCountInterceptor.getQueryCount());
        assertEquals(200, status);
        assertEquals(teacher1.getId(), courseRepository.findById(course.getId()).orElseThrow().getSupervisor().getId());
        teacherIdsToRemove.add(teacher1.getId());
    }

    @Test
    @WithUserDetails("admin")
    void addLessonOnCourseByIdAndLessonIdSuccessful() throws Exception {
        Lesson lesson = new Lesson();
        lesson.setName("test lesson");
        lesson = lessonRepository.save(lesson);

        String uri = "/courses/%s/lessons";
        String inputJson = mapToJson(lesson.getId());
        hibernateQueryCountInterceptor.startCounter();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(String.format(uri, course.getId()))
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(4, hibernateQueryCountInterceptor.getQueryCount());
        assertEquals(200, status);
        lesson = lessonRepository.findById(lesson.getId()).orElseThrow();
        assertEquals(lesson.getCourse().getId(), course.getId());
    }

    @Test
    @WithUserDetails("admin")
    void makeCourseCopyByIdSuccessful() throws Exception {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        String uri = "/courses/%s/make-copy";
        hibernateQueryCountInterceptor.startCounter();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(String.format(uri, course.getId()))
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        CourseDto courseDto = mapFromJson(content, CourseDto.class);
        assertEquals(4, hibernateQueryCountInterceptor.getQueryCount());
        assertNotEquals(courseDto.getId(), course.getId());
        assertEquals(courseRepository.findById(courseDto.getId()).orElseThrow().getName(), course.getName());

    }

    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    protected <T> T mapFromJson(String json, Class<T> clazz)
            throws JsonParseException, JsonMappingException, IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, clazz);
    }
}