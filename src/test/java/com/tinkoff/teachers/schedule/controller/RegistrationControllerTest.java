package com.tinkoff.teachers.schedule.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tinkoff.teachers.schedule.ApplicationIntegrationTest;
import com.tinkoff.teachers.schedule.dto.RegisteredTeacherDto;
import com.tinkoff.teachers.schedule.repository.TeacherRepository;
import com.yannbriancon.interceptor.HibernateQueryCountInterceptor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class RegistrationControllerTest extends ApplicationIntegrationTest {

    private MockMvc mvc;

    @Autowired
    private HibernateQueryCountInterceptor hibernateQueryCountInterceptor;

    @Autowired
    WebApplicationContext webApplicationContext;

    @Autowired
    private TeacherRepository teacherRepository;

    private List<Long> teacherIdsToRemove;

    private RegisteredTeacherDto registeredTeacherDto;

    @BeforeEach
    void setUp() {
        teacherIdsToRemove = new ArrayList<>();
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        registeredTeacherDto = new RegisteredTeacherDto();
        registeredTeacherDto.setFullName("add teacher test");
        registeredTeacherDto.setAge(20);
        registeredTeacherDto.setPassword("1Aqwerty");
        registeredTeacherDto.setUsername("teacher");
        hibernateQueryCountInterceptor.removeCounter();
    }

    @AfterEach
    void tearDown() {
        teacherRepository.deleteAllById(teacherIdsToRemove);
    }

    @Test
    void registerUserSuccessful() throws Exception {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        String uri = "/registration";
        String inputJson = mapToJson(registeredTeacherDto);
        hibernateQueryCountInterceptor.startCounter();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(3, hibernateQueryCountInterceptor.getQueryCount());
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();

        registeredTeacherDto = mapFromJson(content, RegisteredTeacherDto.class);
        assertTrue(teacherRepository.existsById(registeredTeacherDto.getId()));
        teacherIdsToRemove.add(registeredTeacherDto.getId());
    }

    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    protected <T> T mapFromJson(String json, Class<T> clazz)
            throws JsonParseException, JsonMappingException, IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, clazz);
    }
}