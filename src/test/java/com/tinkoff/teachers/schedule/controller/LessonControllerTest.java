package com.tinkoff.teachers.schedule.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tinkoff.teachers.schedule.ApplicationIntegrationTest;
import com.tinkoff.teachers.schedule.dto.LessonDto;
import com.tinkoff.teachers.schedule.entity.Lesson;
import com.tinkoff.teachers.schedule.entity.Teacher;
import com.tinkoff.teachers.schedule.mapper.LessonMapper;
import com.tinkoff.teachers.schedule.repository.LessonRepository;
import com.tinkoff.teachers.schedule.repository.TeacherRepository;
import com.yannbriancon.interceptor.HibernateQueryCountInterceptor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class LessonControllerTest extends ApplicationIntegrationTest {

    private MockMvc mvc;

    @Autowired
    private HibernateQueryCountInterceptor hibernateQueryCountInterceptor;

    @Autowired
    WebApplicationContext webApplicationContext;

    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private LessonMapper lessonMapper;

    private Lesson lesson;

    @Autowired
    private TeacherRepository teacherRepository;

    private List<Long> teacherIdsToRemove;

    @BeforeEach
    void setUp() {
        teacherIdsToRemove = new ArrayList<>();
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        lesson = new Lesson();
        lesson.setName("lesson controller test lesson");
        lesson = lessonRepository.save(lesson);
        hibernateQueryCountInterceptor.removeCounter();
    }

    @AfterEach
    void tearDown() {
        lessonRepository.deleteAll();
        teacherRepository.deleteAllById(teacherIdsToRemove);
    }

    @Test
    @WithUserDetails("admin")
    void addLessonSuccessful() throws Exception {
        LessonDto lessonDto = new LessonDto();
        lessonDto.setName("add lesson test");
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        String uri = "/lessons";
        String inputJson = mapToJson(lessonDto);
        hibernateQueryCountInterceptor.startCounter();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(1, hibernateQueryCountInterceptor.getQueryCount());
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();

        lessonDto = mapFromJson(content, LessonDto.class);
        assertTrue(lessonRepository.existsById(lessonDto.getId()));
    }

    @Test
    void getLessonByIdSuccessful() throws Exception {
        String uri = "/lessons/%s";
        hibernateQueryCountInterceptor.startCounter();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(String.format(uri, lesson.getId()))
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        assertEquals(1, hibernateQueryCountInterceptor.getQueryCount());
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(lessonMapper.fromEntity(lesson), mapFromJson(content, LessonDto.class));
    }

    @Test
    void getAllLessonsSuccessful() throws Exception {
        String uri = "/lessons";
        hibernateQueryCountInterceptor.startCounter();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        assertEquals(1, hibernateQueryCountInterceptor.getQueryCount());
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(mapToJson(List.of(lessonMapper.fromEntity(lesson))), content);
    }

    @Test
    @WithUserDetails("admin")
    void updateLessonByIdAndDtoSuccessful() throws Exception {
        lesson.setName("New lesson name");
        String uri = "/lessons/%s";
        String inputJson = mapToJson(lessonMapper.fromEntity(lesson));
        hibernateQueryCountInterceptor.startCounter();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(String.format(uri, lesson.getId()), inputJson)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(2, hibernateQueryCountInterceptor.getQueryCount());
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("New lesson name", mapFromJson(content, LessonDto.class).getName());
    }

    @Test
    @WithUserDetails("admin")
    void deleteTeacherByIdSuccessful() throws Exception {
        String uri = "/lessons";
        String inputJson = mapToJson(lesson.getId());
        hibernateQueryCountInterceptor.startCounter();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(2, hibernateQueryCountInterceptor.getQueryCount());
        assertEquals(200, status);
        assertEquals(0, lessonRepository.findAll().size());
    }

    @Test
    @WithUserDetails("admin")
    void updateTeacherOnLessonByIdAndTeacherIdSuccessful() throws Exception {
        Teacher teacher1 = new Teacher();
        teacher1.setFullName("testing teacher");
        teacher1.setAge(20);
        teacher1 = teacherRepository.save(teacher1);

        String uri = "/lessons/%s/teacher";
        String inputJson = mapToJson(teacher1.getId());
        hibernateQueryCountInterceptor.startCounter();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(String.format(uri, lesson.getId()))
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(3, hibernateQueryCountInterceptor.getQueryCount());
        assertEquals(200, status);
        assertEquals(lessonRepository.findById(lesson.getId()).orElseThrow().getTeacher().getId(), teacher1.getId());
        teacherIdsToRemove.add(teacher1.getId());
    }

    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    protected <T> T mapFromJson(String json, Class<T> clazz)
            throws JsonParseException, JsonMappingException, IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, clazz);
    }
}