package com.tinkoff.teachers.schedule.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tinkoff.teachers.schedule.ApplicationIntegrationTest;
import com.tinkoff.teachers.schedule.dto.LessonDto;
import com.tinkoff.teachers.schedule.dto.TeacherDto;
import com.tinkoff.teachers.schedule.entity.Lesson;
import com.tinkoff.teachers.schedule.entity.Teacher;
import com.tinkoff.teachers.schedule.mapper.LessonMapper;
import com.tinkoff.teachers.schedule.mapper.TeacherMapper;
import com.tinkoff.teachers.schedule.repository.LessonRepository;
import com.tinkoff.teachers.schedule.repository.TeacherRepository;
import com.yannbriancon.interceptor.HibernateQueryCountInterceptor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class TeacherControllerTest extends ApplicationIntegrationTest {

    private MockMvc mvc;

    @Autowired
    private HibernateQueryCountInterceptor hibernateQueryCountInterceptor;

    @Autowired
    WebApplicationContext webApplicationContext;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private TeacherMapper teacherMapper;

    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private LessonMapper lessonMapper;

    private List<Long> teacherIdsToRemove;

    private Teacher teacher;

    @BeforeEach
    void setUp() {
        teacherIdsToRemove = new ArrayList<>();
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        teacher = new Teacher();
        teacher.setFullName("teacher controller test teacher");
        teacher.setAge(20);
        teacher.setPassword("$2a$12$veLzzZkPEFee57X0327a1.v675.X3aD29ElUBvys9FwaakD66uOgq");
        teacher.setUsername("teacher");
        teacher = teacherRepository.save(teacher);
        teacherIdsToRemove.add(teacher.getId());
        hibernateQueryCountInterceptor.removeCounter();
    }

    @AfterEach
    void tearDown() {
        lessonRepository.deleteAll();
        teacherRepository.deleteAllById(teacherIdsToRemove);
    }

    @Test
    void getTeacherByIdSuccessful() throws Exception {
        String uri = "/teachers/%s";
        hibernateQueryCountInterceptor.startCounter();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(String.format(uri, teacher.getId()))
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(1, hibernateQueryCountInterceptor.getQueryCount());
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(teacherMapper.fromEntity(teacher), mapFromJson(content, TeacherDto.class));
    }

    @Test
    void getAllTeachersSuccessful() throws Exception {
        Teacher teacher1 = new Teacher();
        teacher1.setId(1L);
        teacher1.setFullName("admin");

        String uri = "/teachers";
        hibernateQueryCountInterceptor.startCounter();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(1, hibernateQueryCountInterceptor.getQueryCount());
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(mapToJson(List.of(teacherMapper.fromEntity(teacher1), teacherMapper.fromEntity(teacher))), content);
    }

    @Test
    @WithUserDetails("admin")
    void updateTeacherByIdAndDtoSuccessful() throws Exception {
        teacher.setFullName("New teacher name");
        String uri = "/teachers/%s";
        String inputJson = mapToJson(teacherMapper.fromEntity(teacher));
        hibernateQueryCountInterceptor.startCounter();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(String.format(uri, teacher.getId()), inputJson)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(2, hibernateQueryCountInterceptor.getQueryCount());
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("New teacher name", mapFromJson(content, TeacherDto.class).getFullName());
    }

    @Test
    @WithUserDetails("admin")
    void deleteTeacherByIdSuccessful() throws Exception {
        String uri = "/teachers";
        String inputJson = mapToJson(teacher.getId());
        hibernateQueryCountInterceptor.startCounter();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        assertEquals(4, hibernateQueryCountInterceptor.getQueryCount());
        assertEquals(1, teacherRepository.findAll().size());
        teacherIdsToRemove.remove(teacher.getId());
    }

    @Test
    @WithUserDetails("admin")
    void getLessonsByTeacherIdSuccessful() throws Exception {
        Lesson lesson1 = new Lesson();
        lesson1.setTeacher(teacher);
        lessonRepository.save(lesson1);
        teacher = teacherRepository.save(teacher);

        String uri = "/teachers/%s/lessons";
        hibernateQueryCountInterceptor.startCounter();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(String.format(uri, teacher.getId()))
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        assertEquals(2, hibernateQueryCountInterceptor.getQueryCount());
        String content = mvcResult.getResponse().getContentAsString();

        List<LessonDto> lessons = Arrays.stream(mapFromJson(content, LessonDto[].class)).toList();
        assertEquals(List.of(lessonMapper.fromEntity(lesson1)), lessons);

    }

    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    protected <T> T mapFromJson(String json, Class<T> clazz)
            throws JsonParseException, JsonMappingException, IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, clazz);
    }
}