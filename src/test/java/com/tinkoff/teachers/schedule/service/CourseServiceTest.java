package com.tinkoff.teachers.schedule.service;

import com.tinkoff.teachers.schedule.ApplicationIntegrationTest;
import com.tinkoff.teachers.schedule.dto.CourseDto;
import com.tinkoff.teachers.schedule.entity.*;
import com.tinkoff.teachers.schedule.mapper.CourseMapper;
import com.tinkoff.teachers.schedule.mapper.LessonMapper;
import com.tinkoff.teachers.schedule.repository.CourseRepository;
import com.tinkoff.teachers.schedule.repository.LessonRepository;
import com.tinkoff.teachers.schedule.repository.TeacherRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CourseServiceTest extends ApplicationIntegrationTest {

    @Autowired
    private CourseService courseService;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private CourseMapper courseMapper;

    private Course course;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private LessonMapper lessonMapper;

    private List<Long> teacherIdsToRemove;

    @BeforeEach
    void setUp() {
        course = new Course();
        course.setName("test course");
        course.setDescription("description test course");
        course.setCategory(CourseCategory.INFORMATICS);
        course.setType(CourseType.ONLINE);
        course.setAdditionalInformation("test course link");
        course.setNumberOfStudents(0);
        course = courseRepository.save(course);
        teacherIdsToRemove = new ArrayList<>();
    }

    @AfterEach
    void tearDown() {
        lessonRepository.deleteAll();
        courseRepository.deleteAll();
        teacherRepository.deleteAllById(teacherIdsToRemove);
    }

    @Test
    @WithUserDetails("admin")
    void addCourse() {
        CourseDto courseDto = new CourseDto();
        courseDto.setName("addLessonTest name");
        courseDto.setDescription("description test course");
        courseDto.setCategory(CourseCategory.INFORMATICS);
        courseDto.setType(CourseType.ONLINE);
        courseDto.setAdditionalInformation("test course link");
        courseDto.setNumberOfStudents(0);
        courseDto = courseService.add(courseDto);
        assertTrue(courseRepository.existsById(courseDto.getId()));
        Course course1 = courseRepository.findById(courseDto.getId()).orElseThrow();
        assertEquals(course1.getName(), courseDto.getName());
        assertEquals(course1.getDescription(), courseDto.getDescription());
        assertEquals(course1.getCategory(), courseDto.getCategory());
        assertEquals(course1.getType(), courseDto.getType());
        assertEquals(course1.getAdditionalInformation(), courseDto.getAdditionalInformation());
        assertEquals(course1.getNumberOfStudents(), courseDto.getNumberOfStudents());
        assertEquals("admin", courseDto.getSupervisor().getFullName());
    }

    @Test
    void getCourseByIdWhenCourseDoesNotExist() {
        EntityNotFoundException exception = assertThrows(EntityNotFoundException.class, () -> courseService.getById(0L));
        assertEquals("Course not found by id = 0", exception.getMessage());
    }

    @Test
    void getCourseById() {
        assertEquals(course.getId(), courseService.getById(course.getId()).getId());
    }

    @Test
    void getAllLessons() {
        assertEquals(courseService.getAll(), List.of(courseMapper.fromEntity(course)));
    }

    @Test
    void updateLessonByIdAndDto() {
        CourseDto courseDto = courseMapper.fromEntity(course);
        courseDto.setName("updated name");
        courseDto.setDescription("updated description test course");
        courseDto.setCategory(CourseCategory.MATH);
        courseDto.setType(CourseType.OFFLINE);
        courseDto.setAdditionalInformation("test course address");
        courseDto.setNumberOfStudents(1);
        courseService.update(courseDto.getId(), courseDto);
        CourseDto updatedCourseDto = courseService.getById(courseDto.getId());
        assertEquals("updated name", updatedCourseDto.getName());
        assertEquals("updated description test course", updatedCourseDto.getDescription());
        assertEquals(CourseCategory.MATH, updatedCourseDto.getCategory());
        assertEquals(CourseType.OFFLINE, updatedCourseDto.getType());
        assertEquals("test course address", updatedCourseDto.getAdditionalInformation());
        assertEquals(1, updatedCourseDto.getNumberOfStudents());
    }

    @Test
    @WithUserDetails("admin")
    void deleteLessonById() {
        courseService.deleteById(course.getId());
        assertFalse(courseRepository.existsById(course.getId()));
    }

    @Test
    void updateSupervisorOnCourseByIdAndTeacherId() {
        Teacher teacher1 = new Teacher();
        teacher1.setFullName("testing teacher");
        teacher1.setAge(20);
        teacher1 = teacherRepository.save(teacher1);
        courseService.updateSupervisor(course.getId(), teacher1.getId());
        assertEquals(courseRepository.findById(course.getId()).orElseThrow().getSupervisor().getId(), teacher1.getId());
        teacherIdsToRemove.add(teacher1.getId());
    }

    @Test
    void addLessonOnCourseByIdAndLessonId() {
        Lesson lesson = new Lesson();
        lesson.setName("test lesson");
        lesson = lessonRepository.save(lesson);
        courseService.addLesson(course.getId(), lesson.getId());
        lesson = lessonRepository.findById(lesson.getId()).orElseThrow();
        assertEquals(lesson.getCourse().getId(), course.getId());
    }

    @Test
    void getLessonsByCourseId() {
        Lesson lesson = new Lesson();
        lesson.setName("test lesson");
        lesson.setCourse(course);
        lesson = lessonRepository.save(lesson);
        course.getLessons().add(lesson);
        courseRepository.save(course);
        Set<Lesson> lessons = course.getLessons();
        assertEquals(Set.of(lesson), lessons);
    }

    @Test
    @WithUserDetails("admin")
    void makeCourseCopyByCourseId() {
        Lesson lesson = new Lesson();
        lesson.setName("test lesson");
        lesson.setCourse(course);
        lesson = lessonRepository.save(lesson);
        course.getLessons().add(lesson);
        course = courseRepository.save(course);
        CourseDto courseDto = courseService.makeCopyById(course.getId());
        assertNotEquals(courseDto.getId(), course.getId());
        assertEquals(courseRepository.findById(courseDto.getId()).orElseThrow().getName(), course.getName());
    }
}