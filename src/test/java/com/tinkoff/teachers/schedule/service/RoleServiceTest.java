package com.tinkoff.teachers.schedule.service;

import com.tinkoff.teachers.schedule.ApplicationIntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class RoleServiceTest extends ApplicationIntegrationTest {

    @Autowired
    private RoleService roleService;

    @Test
    void getRoleByNameSuccessful() {
        assertEquals("ADMIN", roleService.getByName("ADMIN").getName());
    }

    @Test
    void getRoleByNameWhenRoleDoesNotExist() {
        EntityNotFoundException exception = assertThrows(EntityNotFoundException.class, () -> roleService.getByName("NEWROLE"));
        assertEquals("Not found role: " + "NEWROLE", exception.getMessage());
    }
}