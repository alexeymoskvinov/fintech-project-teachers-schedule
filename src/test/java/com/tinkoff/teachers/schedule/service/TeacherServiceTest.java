package com.tinkoff.teachers.schedule.service;

import com.tinkoff.teachers.schedule.ApplicationIntegrationTest;
import com.tinkoff.teachers.schedule.dto.LessonDto;
import com.tinkoff.teachers.schedule.dto.RegisteredTeacherDto;
import com.tinkoff.teachers.schedule.dto.TeacherDto;
import com.tinkoff.teachers.schedule.entity.Lesson;
import com.tinkoff.teachers.schedule.entity.Role;
import com.tinkoff.teachers.schedule.entity.Teacher;
import com.tinkoff.teachers.schedule.mapper.LessonMapper;
import com.tinkoff.teachers.schedule.mapper.TeacherMapper;
import com.tinkoff.teachers.schedule.repository.LessonRepository;
import com.tinkoff.teachers.schedule.repository.RoleRepository;
import com.tinkoff.teachers.schedule.repository.TeacherRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TeacherServiceTest extends ApplicationIntegrationTest {

    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private TeacherMapper teacherMapper;

    private Teacher teacher1;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private LessonMapper lessonMapper;

    private List<Long> teacherIdsToRemove;

    @BeforeEach
    void setUp() {
        Role role = roleRepository.getById(2L);
        teacher1 = new Teacher();
        teacher1.setFullName("testing teacher");
        teacher1.setAge(20);
        teacher1.setPassword("1Aqwerty");
        teacher1.setRole(role);
        teacher1 = teacherRepository.save(teacher1);
        teacherIdsToRemove = new ArrayList<>();
        teacherIdsToRemove.add(teacher1.getId());
    }

    @AfterEach
    void tearDown() {
        lessonRepository.deleteAll();
        teacherRepository.deleteAllById(teacherIdsToRemove);
    }

    @Test
    void RegistrationTeacher() {
        RegisteredTeacherDto registeredTeacherDto = new RegisteredTeacherDto();
        registeredTeacherDto.setFullName("add teacher test");
        registeredTeacherDto.setAge(20);
        registeredTeacherDto.setPassword("1Aqwerty");
        registeredTeacherDto.setUsername("teacher");
        registeredTeacherDto = teacherService.register(registeredTeacherDto);
        assertTrue(teacherRepository.existsById(registeredTeacherDto.getId()));
        Teacher teacher = teacherRepository.findById(registeredTeacherDto.getId()).orElseThrow();
        assertEquals(registeredTeacherDto.getFullName(), teacher.getFullName());
        assertEquals(registeredTeacherDto.getAge(), teacher.getAge());
        assertEquals(registeredTeacherDto.getUsername(), teacher.getUsername());
        assertTrue(bCryptPasswordEncoder.matches("1Aqwerty", teacher.getPassword()));
        teacherIdsToRemove.add(registeredTeacherDto.getId());
    }


    @Test
    void findTeacherByIdSuccessful() {
        assertEquals(teacher1, teacherService.findById(teacher1.getId()));
    }

    @Test
    void findTeacherByIdWhenTeacherDoesNotExist() {
        EntityNotFoundException exception = assertThrows(EntityNotFoundException.class, () -> teacherService.findById(0L));
        assertEquals("Teacher not found by id=0", exception.getMessage());
    }

    @Test
    void getTeacherById() {
        assertEquals(teacher1.getId(), teacherService.getById(teacher1.getId()).getId());
    }

    @Test
    void getAllTeachers() {
        TeacherDto teacherDto1 = new TeacherDto();
        teacherDto1.setId(1L);
        assertEquals(teacherService.getAll(), List.of(teacherDto1, teacherMapper.fromEntity(teacher1)));
    }

    @Test
    void updateTeacherByIdAndDto() {
        TeacherDto teacherDto1 = teacherMapper.fromEntity(teacher1);
        teacherDto1.setFullName("updated name");
        teacherDto1.setAge(30);
        teacherService.update(teacherDto1.getId(), teacherDto1);
        TeacherDto updatedTeacherDto = teacherService.getById(teacher1.getId());
        assertEquals("updated name", updatedTeacherDto.getFullName());
        assertEquals(30, updatedTeacherDto.getAge());
    }

    @Test
    void deleteTeacherById() {
        teacherService.deleteById(teacher1.getId());
        assertFalse(teacherRepository.existsById(teacher1.getId()));
        teacherIdsToRemove.remove(teacher1.getId());
    }

    @Test
    void loadUserByUsernameSuccessful() {
        assertEquals(teacher1.getUsername(), teacherService.loadUserByUsername(teacher1.getUsername()).getUsername());
    }

    @Test
    void loadUserByUsernameWhenTeacherDoesNotExist() {
        UsernameNotFoundException exception = assertThrows(UsernameNotFoundException.class, () -> teacherService.loadUserByUsername("Name when does not exist"));
        assertEquals("Not found Teacher with username: Name when does not exist", exception.getMessage());
    }

    @Test
    void existsTeacherByUsername() {
        assertTrue(teacherService.existsTeacherByUsername(teacher1.getUsername()));
    }

    @Test
    void getLessonsByTeacherId() {
        Lesson lesson1 = new Lesson();
        lesson1.setDateTime(LocalDateTime.of(2022, 5, 19, 9, 15, 15));
        Lesson lesson2 = new Lesson();
        lesson2.setDateTime(LocalDateTime.of(2022, 5, 19, 17, 15, 15));
        Lesson lesson3 = new Lesson();
        lesson3.setDateTime(LocalDateTime.of(2022, 5, 15, 17, 15, 15));
        lesson1.setTeacher(teacher1);
        lesson2.setTeacher(teacher1);
        lesson3.setTeacher(teacher1);
        lesson1 = lessonRepository.save(lesson1);
        lesson2 = lessonRepository.save(lesson2);
        lesson3 = lessonRepository.save(lesson3);
        teacher1.getLessons().addAll(List.of(lesson1, lesson2, lesson3));
        teacher1 = teacherRepository.save(teacher1);
        LessonDto lessonDto1 = lessonMapper.fromEntity(lesson1);
        LessonDto lessonDto2 = lessonMapper.fromEntity(lesson2);
        LessonDto lessonDto3 = lessonMapper.fromEntity(lesson3);
        assertEquals(List.of(lessonDto3, lessonDto1, lessonDto2), teacherService.getLessonsByTeacherId(teacher1.getId()));
    }
}