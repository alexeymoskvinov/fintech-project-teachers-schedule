package com.tinkoff.teachers.schedule.service;

import com.tinkoff.teachers.schedule.ApplicationIntegrationTest;
import com.tinkoff.teachers.schedule.dto.LessonDto;
import com.tinkoff.teachers.schedule.entity.Lesson;
import com.tinkoff.teachers.schedule.entity.Teacher;
import com.tinkoff.teachers.schedule.mapper.LessonMapper;
import com.tinkoff.teachers.schedule.repository.LessonRepository;
import com.tinkoff.teachers.schedule.repository.RoleRepository;
import com.tinkoff.teachers.schedule.repository.TeacherRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class LessonServiceTest extends ApplicationIntegrationTest {

    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private LessonService lessonService;

    @Autowired
    private LessonMapper lessonMapper;

    @Autowired
    private TeacherRepository teacherRepository;

    private Lesson lesson;

    @Autowired
    RoleRepository roleRepository;

    private List<Long> teacherIdsToRemove;

    @BeforeEach
    void setUp() {
        teacherIdsToRemove = new ArrayList<>();
        lesson = new Lesson();
        lesson.setName("test lesson");
        lesson.setDescription("description of test lesson");
        lesson.setDateTime(LocalDateTime.of(2022, 5, 19, 9, 15, 15));
        lesson = lessonRepository.save(lesson);
    }

    @AfterEach
    void tearDown() {
        lessonRepository.deleteAll();
        teacherRepository.deleteAllById(teacherIdsToRemove);
    }

    @Test
    @WithUserDetails("admin")
    void addLesson() {
        LessonDto lessonDto = new LessonDto();
        lessonDto.setName("addLessonTest name");
        lessonDto.setDescription("addLessonTest description");
        lessonDto.setDateTime(LocalDateTime.of(2022, 5, 19, 9, 15, 15));
        lessonDto = lessonService.add(lessonDto);
        assertTrue(lessonRepository.existsById(lessonDto.getId()));
        Lesson lesson1 = lessonRepository.findById(lessonDto.getId()).orElseThrow();
        assertEquals(lessonDto.getName(), lesson1.getName());
        assertEquals(lessonDto.getDescription(), lesson1.getDescription());
        assertEquals(lessonDto.getDateTime(), lesson1.getDateTime());
    }

    @Test
    void findLessonByIdSuccessful() {
        assertEquals(lesson, lessonService.findById(lesson.getId()));
    }

    @Test
    void findLessonByIdWhenLessonDoesNotExist() {
        EntityNotFoundException exception = assertThrows(EntityNotFoundException.class, () -> lessonService.findById(0L));
        assertEquals("Lesson not found by id=0", exception.getMessage());
    }

    @Test
    void getLessonById() {
        assertEquals(lesson.getId(), lessonService.getById(lesson.getId()).getId());
    }

    @Test
    void getAllLessons() {
        assertEquals(lessonService.getAll(), List.of(lessonMapper.fromEntity(lesson)));
    }

    @Test
    void updateLessonByIdAndDto() {
        LessonDto lessonDto = lessonMapper.fromEntity(lesson);
        lessonDto.setName("updated name");
        lessonDto.setDescription("updated description");
        lessonDto.setDateTime(LocalDateTime.of(2020, 7, 15, 3, 10, 12));
        lessonService.update(lessonDto.getId(), lessonDto);
        LessonDto updatedLessonDto = lessonService.getById(lessonDto.getId());
        assertEquals("updated name", updatedLessonDto.getName());
        assertEquals("updated description", updatedLessonDto.getDescription());
        assertEquals(LocalDateTime.of(2020, 7, 15, 3, 10, 12), updatedLessonDto.getDateTime());
    }

    @Test
    @WithUserDetails("admin")
    void deleteLessonById() {
        lessonService.deleteById(lesson.getId());
        assertFalse(lessonRepository.existsById(lesson.getId()));
    }

    @Test
    void updateTeacherOnLessonByIdAndTeacherId() {
        Teacher teacher1 = new Teacher();
        teacher1.setFullName("testing teacher");
        teacher1.setAge(20);
        teacher1 = teacherRepository.save(teacher1);
        lessonService.updateTeacher(lesson.getId(), teacher1.getId());
        assertEquals(lessonRepository.findById(lesson.getId()).orElseThrow().getTeacher().getId(), teacher1.getId());
        teacherIdsToRemove.add(teacher1.getId());
    }
}