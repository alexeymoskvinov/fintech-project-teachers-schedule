package com.tinkoff.teachers.schedule.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tinkoff.teachers.schedule.ApplicationIntegrationTest;
import com.tinkoff.teachers.schedule.dto.RegisteredTeacherDto;
import com.tinkoff.teachers.schedule.entity.Course;
import com.tinkoff.teachers.schedule.entity.Teacher;
import com.tinkoff.teachers.schedule.mapper.CourseMapper;
import com.tinkoff.teachers.schedule.repository.CourseRepository;
import com.tinkoff.teachers.schedule.repository.TeacherRepository;
import com.tinkoff.teachers.schedule.service.CourseService;
import com.tinkoff.teachers.schedule.service.TeacherService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@SpringBootTest
class SecurityTest extends ApplicationIntegrationTest {

    private MockMvc mvc;

    @Autowired
    WebApplicationContext webApplicationContext;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private CourseService courseService;

    @Autowired
    private CourseMapper courseMapper;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private TeacherRepository teacherRepository;

    private Course course1;

    private List<Long> teacherIdsToRemove;

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
        teacherIdsToRemove = new ArrayList<>();
        course1 = new Course();
        course1.setName("security test course");

    }

    @AfterEach
    void tearDown() {
        courseRepository.deleteAll();
        teacherRepository.deleteAllById(teacherIdsToRemove);
    }

    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void createCourseWithAdminStatusOk() throws Exception {
        String uri = "/courses";
        String inputJson = mapToJson(courseMapper.fromEntity(course1));
        String auth = usernameColonPasswordEncoder("admin:admin");
        MvcResult mvcResult = mvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputJson)
                        .header(HttpHeaders.AUTHORIZATION, auth))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
    }

    @Test
    void createCourseWithUnauthorizedUserStatus401() throws Exception {
        String uri = "/courses";
        String inputJson = mapToJson(courseMapper.fromEntity(course1));
        MvcResult mvcResult = mvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputJson)
                        .header(HttpHeaders.AUTHORIZATION, ""))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(401, status);
    }

    @Test
    @Transactional
    void teacherUpdateTeacherNotToHisCourseStatus403() throws Exception {
        course1 = courseRepository.save(course1);
        Teacher teacher1 = new Teacher();
        teacher1.setFullName("testing teacher");
        teacher1.setAge(20);
        teacher1 = teacherRepository.save(teacher1);
        teacherIdsToRemove.add(teacher1.getId());
        courseService.updateSupervisor(course1.getId(), teacher1.getId());

        RegisteredTeacherDto registeredTeacherDto = new RegisteredTeacherDto();
        registeredTeacherDto.setFullName("add teacher test");
        registeredTeacherDto.setAge(20);
        registeredTeacherDto.setUsername("teacher");
        registeredTeacherDto.setPassword("1Aqwerty");
        registeredTeacherDto = teacherService.register(registeredTeacherDto);
        teacherIdsToRemove.add(registeredTeacherDto.getId());

        String uri = "/courses/%s/supervisor";
        String inputJson = mapToJson(1L);
        String auth = usernameColonPasswordEncoder("teacher:1Aqwerty");
        MvcResult mvcResult = mvc
                .perform(MockMvcRequestBuilders
                        .put(String.format(uri, course1.getId()))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputJson)
                        .header(HttpHeaders.AUTHORIZATION, auth))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(403, status);
    }

    @Test
    @Transactional
    void teacherUpdateTeacherHisCourseStatusOk() throws Exception {
        course1 = courseRepository.save(course1);
        RegisteredTeacherDto registeredTeacherDto = new RegisteredTeacherDto();
        registeredTeacherDto.setFullName("add teacher test");
        registeredTeacherDto.setAge(20);
        registeredTeacherDto.setUsername("teacher");
        registeredTeacherDto.setPassword("1Aqwerty");
        registeredTeacherDto = teacherService.register(registeredTeacherDto);
        teacherIdsToRemove.add(registeredTeacherDto.getId());
        courseService.updateSupervisor(course1.getId(), registeredTeacherDto.getId());

        String uri = "/courses/%s/supervisor";
        String inputJson = mapToJson(registeredTeacherDto.getId());
        String auth = usernameColonPasswordEncoder("teacher:1Aqwerty");
        MvcResult mvcResult = mvc
                .perform(MockMvcRequestBuilders
                        .put(String.format(uri, course1.getId()))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputJson)
                        .header(HttpHeaders.AUTHORIZATION, auth))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
    }

    public String usernameColonPasswordEncoder(String usernameColonPassword) {
        return "Basic " + Base64.getEncoder().encodeToString(usernameColonPassword.getBytes(StandardCharsets.UTF_8));
    }
}